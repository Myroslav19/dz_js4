function calculate(num1, num2, operator) {
    switch(operator) {
      case '+':
        return num1 + num2;
      case '-':
        return num1 - num2;
      case '*':
        return num1 * num2;
      case '/':
        return num1 / num2;
      default:
        return NaN; 
    }
  }
  const num1 = parseFloat(prompt('Enter first number:'));
  const num2 = parseFloat(prompt('Enter second number:'));
  const operator = prompt('Enter the operation(+, -, *, /):');
   if (isNaN(num1) || isNaN(num2) || !['+', '-', '*', '/'].includes(operator)) {
   console.log('The entered data is incorrect');
  } else {
    const result = calculate(num1, num2, operator);
    console.log(`${num1} ${operator} ${num2} = ${result}`);
  }